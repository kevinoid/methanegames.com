Archive of methanegames.com and TriPeaksLite
============================================

This repository contains an archived copy of the content from
methanegames.com, including a reimplementation of `/api/TriPeaks` in PHP and a
work-in-progress PowerShell script for retargeting `TriPeaksLite.xap`.

This project is not approved or condoned by Methane Games.  I have attempted
to contact them multiple times via multiple methods without success, both
before and after the site went offline.  As a fan of TriPeaksLite, this is my
attempt to preserve it.
