<?php
/**
 * Partial reimplementation of the TriPeaksLite API.
 *
 * PHP Version 7.3
 *
 * @author  Kevin Locke <kevin@kevinlocke.name>
 * @license CC0 https://creativecommons.org/publicdomain/zero/1.0/
 */

declare(strict_types=1);

/**
 * PDO Data Source Name to database used for data storage.
 */
const DB_DRIVER = 'sqlite';
const DB_DSN = DB_DRIVER . ':' . __DIR__ . '/TriPeaks.sqlite3'
    // If using mysql, use charset utf8mb4 to support Astral UTF-8
    // Ensure database has been created with CHARACTER SET utf8mb4
    . (DB_DRIVER === 'mysql' ? ';charset=utf8mb4' : '');
const DB_USERNAME = '';
const DB_PASSWORD = '';

// Note: Best practice would be to use password_hash() with Argon2id
// Unfortunately, the API design requires player lookup by name+password,
// so password must hash to same bytes for efficient DB lookup.
// PHP doesn't provide Argon or Scrypt functions, use PBKDF2 with shared salt.
const PBKDF2_HASH_ALGO = 'sha512';
const PBKDF2_ITERATIONS = 10000;
const PBKDF2_SALT
    = '\x74\xde\x73\xb6\x73\x8a\x63\xc5\x10\x70\x54\xdb\x14\xfb\x44\x87';

// The TriPeaksLite API returns the player's password in API responses.
// This is a significant security problem, since any player can get the
// passwords for all other players (from the leader board or by ID).
//
// This constant defines the value of the Password property which will be sent
// in API responses for compatibility.
//
// This value is also ignored in UpdatePassword requests, so that player
// passwords aren't automatically set to this value when the client sends
// an update (e.g. to update the score).  Since the TriPeaksLite.xap doesn't
// provide a way for users to update passwords, this isn't a big issue in
// practice.
//
// Use the empty string, since the TriPeaksLite.xap doesn't allow players to
// be created with empty passwords.
//
// IMPORTANT: Must not contain characters which need escaping in SQL string
// literals.  (Without refactoring code below.)
const PASSWORD_PLACEHOLDER = '';

// Note: PDO::PARAM_STR_CHAR and PDO::PARAM_STR_NATL are not used here.
// VARCHAR and '' are interpreted in configured charset
// NVARCHAR and N'' are interpreted as Unicode on SQL Server and MariaDB/MySQL
// Unfortunately, PARAM_STR_NATL forces utf8 (not utf8mb4) on MariaDB/MySQL
// See https://dev.mysql.com/doc/refman/8.0/en/string-literals.html
// So it's preferable to ensure the database charset is configured correctly
// and use VARCHAR instead of using NVARCHAR.
const PLAYER_COLUMNS_UPDATE = [
    'TriPeaksPlayerModelID' => PDO::PARAM_INT,
    'Name_Identifier' => PDO::PARAM_STR,
    'Name' => PDO::PARAM_STR,
    'Email' => PDO::PARAM_STR,
    'Rank' => PDO::PARAM_INT,
    // PDO doesn't have a parameter type for float/double
    // https://bugs.php.net/43203
    'Score' => PDO::PARAM_STR,
    'Streak' => PDO::PARAM_INT,
    'GamesPlayed' => PDO::PARAM_INT,
    'MostWon' => PDO::PARAM_INT,
    'MostLost' => PDO::PARAM_INT,
    'Cleared' => PDO::PARAM_INT,
    'Months' => PDO::PARAM_INT,
    'Lastdate' => PDO::PARAM_INT,
    'Status' => PDO::PARAM_INT,
    'Registration' => PDO::PARAM_STR,
    'Tokens' => PDO::PARAM_INT,
    'Version' => PDO::PARAM_INT,
    'Relationship' => PDO::PARAM_INT,
    'Res1' => PDO::PARAM_STR,
    'Res2' => PDO::PARAM_STR,
];
const PLAYER_COLUMNS_SELECT = <<<'SQL'
    "TriPeaksPlayerModelID",
    "Name_Identifier",
    "Name",
    "Email",
    "Rank",
    "Score",
    "Streak",
    "GamesPlayed",
    "MostWon",
    "MostLost",
    "Cleared",
    "Months",
    "Lastdate",
    '
SQL . PASSWORD_PLACEHOLDER . <<<'SQL'
' AS "Password",
    "Status",
    "Registration",
    "Tokens",
    "Version",
    "Relationship",
    "Res1",
    "Res2"
SQL;

/**
 * Map request path and method to a handler function.
 */
const API_PATH_METHOD_HANDLER = [
    '/GetPlayerByNameID' => [ 'POST' => 'getPlayerByNameID' ],
    '/GetPlayerByNamePassword' => [ 'POST' => 'getPlayerByNamePassword' ],
    '/GetPlayerRangeByName' => [ 'POST' => 'getPlayerRangeByName' ],
    '/GetPlayerRangeByScore' => [ 'POST' => 'getPlayerRangeByScore' ],
    '/GetPlayerRangeFromTop' => [ 'POST' => 'getPlayerRangeFromTop' ],
    '/GetScoreRank' => [ 'POST' => 'getScoreRank' ],
    // Note: Custom API method to initialize the database
    '/InitializeDB' => [ 'POST' => 'initializeDB' ],
    '/UpdatePlayer' => [ 'POST' => 'updatePlayer' ],
];


/**
 * Hashes a password in a way suitable for use as a search key, with behavior
 * matching the TriPeaks API.
 *
 * @param string $password  Password provided by user.
 * @param bool   $rawOutput When set to TRUE (default), outputs raw binary data.
 *                          FALSE outputs lowercase hexits.
 *
 * @return string Binary or hex string of PBKDF2 hash of lower-cased, trimmed
 * $password.
 */
function hashPassword(string $password, bool $rawOutput=true) : string
{
    return hash_pbkdf2(
        PBKDF2_HASH_ALGO,
        // TriPeaks matches password case- and padding-insensitively
        strtolower(rtrim($password, ' ')),
        PBKDF2_SALT,
        PBKDF2_ITERATIONS,
        0,
        $rawOutput,
    );
}


/**
 * Checks if a given Content-Type header value is JSON.
 *
 * @param string $contentType Value of Content-Type header.
 *
 * @return bool True if $contentType is JSON, otherwise false.
 */
function isJson(string $contentType) : bool
{
    $semiPos = strpos($contentType, ';');
    $mediaType = $semiPos === false ? $contentType
        : substr($contentType, 0, $semiPos);
    $mediaType = strtolower(trim($mediaType));
    // Note: methanegames.com only recognizes application/json
    return $mediaType === 'application/json';
}


/**
 * Gets a PDO object connected to the configured database.
 *
 * @param bool $canWrite Gets a connection capable of writing to the DB.
 *
 * @return PDO Database connection.
 */
function getDBConnection(bool $canWrite=true)
{
    switch (DB_DRIVER) {
    case 'mysql':
        $options = [
            // Don't emulate prepare, which casts all results to strings
            // https://stackoverflow.com/a/20123337
            PDO::ATTR_EMULATE_PREPARES => false,
        ];
        break;
    case 'sqlite':
        $options = [];
        if (!$canWrite) {
            $options = [
                PDO::SQLITE_ATTR_OPEN_FLAGS => PDO::SQLITE_OPEN_READONLY,
            ];
        }
        break;
    default:
        $options = [];
        break;
    }

    $db = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD, $options);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $db;
}


/**
 * Creates a Player array with values converted to the correct types.
 *
 * Occurs on sqlite: https://bugs.php.net/38334
 * Occurs on mysql without ATTR_EMULATE_PREPARES:
 * https://stackoverflow.com/a/20123337
 *
 * @param array $player Player array to cast.
 *
 * @return array A copy of $player with values of the correct types.
 */
function castPlayer(array $player) : array
{
    if (is_int($player['TriPeaksPlayerModelID'])) {
        // If PDO driver supports int, only need to fix float (e.g. pgsql)
        $value = $player['Score'];
        assert(is_numeric($value));
        $player['Score'] = (float) $value;
        return $player;
    }

    foreach ($player as $key => &$value) {
        if ($key === 'Score') {
            // Cast to float, which doesn't have PDO::PARAM_ constant
            assert(is_numeric($value));
            $value = (float) $value;
            continue;
        }

        switch (PLAYER_COLUMNS_UPDATE[$key] ?? null) {
        case PDO::PARAM_BOOL:
            assert($value === '1' or $value === '0');
            $value = $value === '1' ? true : false;
            break;
        case PDO::PARAM_INT:
            $intval = (int) $value;
            assert($value === (string) $intval);
            $value = $intval;
            break;
        }
    }

    return $player;
}


/**
 * Creates an array of Players with values converted to the correct types.
 *
 * @param array $players Array of Players to cast.
 *
 * @return array A new array which contains new Players with correct types.
 */
function castPlayers(array $players) : array
{
    // If driver supports float, it supports everything, casting not required
    return empty($players) || is_float($players[0]['Score']) ? $players
        : array_map('castPlayer', $players);
}


/**
 * Gets player by name and ID.
 *
 * @param array $body Decoded request body.
 *
 * @return array Players with Name_Identifier matching model.Name_Identifier in
 * $body (count 1), or null if none match.
 */
function getPlayerByNameID($body) : ?array
{
    if (!is_array($body)) {
        throw new Exception('Expected JSON request body to be an object', 400);
    }

    $model = $body['model'] ?? null;
    if (!is_array($model)) {
        throw new Exception('Expected model to be an object', 400);
    }

    $nameIdentifier = $model['Name_Identifier'] ?? null;
    if (!is_string($nameIdentifier)) {
        throw new Exception('Expected model.Name_Identifier to be a string', 400);
    }

    $db = getDBConnection(false);

    $playerCols = PLAYER_COLUMNS_SELECT;
    $stmt = $db->prepare(
        <<<SQL
SELECT $playerCols
FROM "Players"
WHERE "LoweredName_Identifier" = :LoweredName_Identifier
SQL,
    );
    $stmt->bindValue(
        ':LoweredName_Identifier',
        strtolower(rtrim($nameIdentifier, ' ')),
        PLAYER_COLUMNS_UPDATE['Name_Identifier'],
    );
    $stmt->execute();

    $players = $stmt->fetchAll(PDO::FETCH_ASSOC);

    // TriPeaks API returns null instead of an empty array
    return empty($players) ? null : castPlayers($players);
}


/**
 * Gets player by name and password.
 *
 * @param array $body Decoded request body.
 *
 * @return array Players with Name and Password matching model.Name and
 * model.Password in $body (count 1), or null if none match.
 */
function getPlayerByNamePassword($body) : ?array
{
    if (!is_array($body)) {
        throw new Exception('Expected JSON request body to be an object', 400);
    }

    $model = $body['model'] ?? null;
    if (!is_array($model)) {
        throw new Exception('Expected model to be an object', 400);
    }

    $name = $model['Name'] ?? null;
    if (!is_string($name)) {
        throw new Exception('Expected model.Name to be a string', 400);
    }

    $password = $model['Password'] ?? null;
    if (!is_string($password)) {
        throw new Exception('Expected model.Password to be a string', 400);
    }

    $db = getDBConnection(false);
    $playerCols = PLAYER_COLUMNS_SELECT;
    $selectStmt = $db->prepare(
        <<<SQL
SELECT $playerCols
FROM "Players"
WHERE "LoweredName" = :LoweredName AND "HashedPassword" = :HashedPassword
ORDER BY "TriPeaksPlayerModelID"
SQL,
    );
    $selectStmt->bindValue(
        ':LoweredName',
        // TriPeaks matches name case- and padding-insensitively
        strtolower(rtrim($name, ' ')),
        PLAYER_COLUMNS_UPDATE['Name'],
    );
    $selectStmt->bindValue(
        ':HashedPassword',
        hashPassword($password),
        PDO::PARAM_LOB,
    );
    $selectStmt->execute();

    $players = $selectStmt->fetchAll(PDO::FETCH_ASSOC);

    // TriPeaks API returns null instead of an empty array
    return empty($players) ? null : castPlayers($players);
}


/**
 * Gets range of players by score near a given name.
 *
 * @param array $body Decoded request body.
 *
 * @return array Players adjacent to model.Name_Identifier in the score table
 * (count will always be less than or equal to $body['range']) or null if
 * the player matching Name_Identifier is not found.
 */
function getPlayerRangeByName($body) : ?array
{
    if (!is_array($body)) {
        throw new Exception('Expected JSON request body to be an object', 400);
    }

    $range = $body['range'] ?? null;
    if (!is_int($range) or $range <= 0) {
        throw new Exception('Expected range to be a positive integer', 400);
    }

    $model = $body['model'] ?? null;
    if (!is_array($model)) {
        throw new Exception('Expected model to be an object', 400);
    }

    $nameIdentifier = $model['Name_Identifier'] ?? null;
    if (!is_string($nameIdentifier)) {
        throw new Exception('Expected model.Name_Identifier to be a string', 400);
    }

    $db = getDBConnection(false);

    $playerCols = PLAYER_COLUMNS_SELECT;
    $stmt = $db->prepare(
        <<<SQL
SELECT $playerCols
FROM "Players"
WHERE "Name_Identifier" >= :Name_Identifier
ORDER BY "Name_Identifier"
LIMIT 1
SQL,
    );
    $stmt->bindValue(
        ':Name_Identifier',
        $nameIdentifier,
        PLAYER_COLUMNS_UPDATE['Name_Identifier'],
    );
    $stmt->execute();
    $playersMatch = $stmt->fetchAll(PDO::FETCH_ASSOC);
    if (empty($playersMatch)
        or substr_compare(
            $playersMatch[0]['Name_Identifier'],
            $nameIdentifier,
            0,
            strlen($nameIdentifier)
        ) !== 0
    ) {
        // TriPeaks API returns null instead of an empty array
        return null;
    }

    assert(count($playersMatch) === 1, 'Name_Identifier is unique');
    $loweredNameIdentifier
        = strtolower(rtrim($playersMatch[0]['Name_Identifier'], ' '));
    $score = $playersMatch[0]['Score'];
    assert(is_numeric($score), 'Score is a number');

    $stmt = $db->prepare(
        <<<SQL
SELECT $playerCols
FROM "Players"
WHERE "Score" > CAST(:Score AS FLOAT)
    OR ("Score" = CAST(:Score2 AS FLOAT)
        AND "LoweredName_Identifier" > :LoweredName_Identifier)
ORDER BY "Score" ASC, "LoweredName_Identifier" ASC
LIMIT :Limit
SQL,
    );
    $stmt->bindValue(':Score', $score, PLAYER_COLUMNS_UPDATE['Score']);
    // Can't repeat params on mysql non-emulated: https://bugs.php.net/69257
    $stmt->bindValue(':Score2', $score, PLAYER_COLUMNS_UPDATE['Score']);
    $stmt->bindValue(
        ':LoweredName_Identifier',
        $loweredNameIdentifier,
        PLAYER_COLUMNS_UPDATE['Name_Identifier'],
    );
    $stmt->bindValue(':Limit', ceil(($range - 1) / 2), PDO::PARAM_INT);
    $stmt->execute();
    $playersAbove = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $stmt = $db->prepare(
        <<<SQL
SELECT $playerCols
FROM "Players"
WHERE "Score" < CAST(:Score AS FLOAT)
    OR ("Score" = CAST(:Score2 AS FLOAT)
        AND "LoweredName_Identifier" < :LoweredName_Identifier)
ORDER BY "Score" DESC, "LoweredName_Identifier" DESC
LIMIT :Limit
SQL,
    );
    $stmt->bindValue(':Score', $score, PLAYER_COLUMNS_UPDATE['Score']);
    // Can't repeat params on mysql non-emulated: https://bugs.php.net/69257
    $stmt->bindValue(':Score2', $score, PLAYER_COLUMNS_UPDATE['Score']);
    $stmt->bindValue(
        ':LoweredName_Identifier',
        $loweredNameIdentifier,
        PLAYER_COLUMNS_UPDATE['Name_Identifier'],
    );
    $stmt->bindValue(
        ':Limit',
        $range - count($playersAbove) - 1,
        PDO::PARAM_INT,
    );
    $stmt->execute();
    $playersBelow = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $players = array_merge(
        array_reverse($playersAbove),
        $playersMatch,
        $playersBelow,
    );

    return empty($players) ? null : castPlayers($players);
}


/**
 * Gets range of players by score.
 *
 * @param array $body Decoded request body.
 *
 * @return array Top $body['range'] players with Score <= $body['score'] in
 * the score table, or null if none are found.
 */
function getPlayerRangeByScore($body) : ?array
{
    if (!is_array($body)) {
        throw new Exception('Expected JSON request body to be an object', 400);
    }

    $range = $body['range'] ?? null;
    if (!is_int($range) or $range <= 0) {
        throw new Exception('Expected range to be a positive integer', 400);
    }

    $score = $body['score'] ?? null;
    if (!is_numeric($score)) {
        throw new Exception('Expected score to be numeric', 400);
    }

    $db = getDBConnection(false);

    $playerCols = PLAYER_COLUMNS_SELECT;
    $stmt = $db->prepare(
        <<<SQL
SELECT $playerCols
FROM "Players"
WHERE "Score" <= CAST(:Score AS FLOAT)
ORDER BY "Score" DESC, "LoweredName_Identifier" DESC
LIMIT :Limit
SQL,
    );
    $stmt->bindValue(':Score', $score, PLAYER_COLUMNS_UPDATE['Score']);
    $stmt->bindValue(':Limit', $range, PDO::PARAM_INT);
    $stmt->execute();

    $players = $stmt->fetchAll(PDO::FETCH_ASSOC);

    // TriPeaks API returns null instead of an empty array
    return empty($players) ? null : castPlayers($players);
}


/**
 * Gets range of players with maximum score.
 *
 * @param array $body Decoded request body.
 *
 * @return array Players at the top of the score table, in sorted order by
 * Score (count will always be less than or equal to $body['range']).
 */
function getPlayerRangeFromTop($body) : ?array
{
    if (!is_array($body)) {
        throw new Exception('Expected JSON request body to be an object', 400);
    }

    $range = $body['range'] ?? null;
    if (!is_int($range) or $range <= 0) {
        throw new Exception('Expected range to be a positive integer', 400);
    }

    $db = getDBConnection(false);

    $playerCols = PLAYER_COLUMNS_SELECT;
    $stmt = $db->prepare(
        <<<SQL
SELECT $playerCols
FROM "Players"
ORDER BY "Score" DESC, "LoweredName_Identifier" DESC
LIMIT :Limit
SQL,
    );
    $stmt->bindValue(':Limit', $range, PDO::PARAM_INT);
    $stmt->execute();

    $players = $stmt->fetchAll(PDO::FETCH_ASSOC);

    return empty($players) ? null : castPlayers($players);
}


/**
 * Gets score and rank.
 *
 * @param array $body POST body.
 *
 * @return int Rank (in score table) of player matching model.Name_Identifier.
 */
function getScoreRank($body) : int
{
    if (!is_array($body)) {
        throw new Exception('Expected JSON request body to be an object', 400);
    }

    $model = $body['model'] ?? null;
    if (!is_array($model)) {
        throw new Exception('Expected model to be an object', 400);
    }

    $nameIdentifier = $model['Name_Identifier'] ?? null;
    if (!is_string($nameIdentifier)) {
        throw new Exception('Expected model.Name_Identifier to be a string', 400);
    }

    $db = getDBConnection(false);

    // Note: In SQLite RIGHT and FULL OUTER JOINs are not currently supported
    $stmt = $db->prepare(
        <<<SQL
SELECT COUNT(*)
FROM (SELECT COALESCE((
                SELECT "Score"
                FROM "Players"
                WHERE "LoweredName_Identifier" = :LoweredName_Identifier
                ), -2147483648) AS "Score") S
    INNER JOIN "Players" P ON P."Score" > S."Score"
        OR (P."Score" = S."Score"
            AND P."LoweredName_Identifier" > :LoweredName_Identifier2)
SQL,
    );
    $stmt->bindValue(
        ':LoweredName_Identifier',
        strtolower(rtrim($nameIdentifier, ' ')),
        PLAYER_COLUMNS_UPDATE['Name_Identifier'],
    );
    // Can't repeat params on mysql non-emulated: https://bugs.php.net/69257
    $stmt->bindValue(
        ':LoweredName_Identifier2',
        strtolower(rtrim($nameIdentifier, ' ')),
        PLAYER_COLUMNS_UPDATE['Name_Identifier'],
    );
    $stmt->execute();
    return (int) $stmt->fetchColumn();
}


/**
 * Removes all data and initialization from the database.
 *
 * @param array $body POST body. (ignored)
 *
 * @return void
 */
function wipeDB($body=null) : void
{
    $db = getDBConnection(true);
    $db->exec(
        <<<SQL
DROP TABLE IF EXISTS "Players";
SQL
    );
}


/**
 * Initializes the database.
 *
 * @param array $body POST body. (ignored)
 *
 * @return void
 */
function initializeDB($body=null) : void
{
    $pkColDef = [
        'mysql' => 'INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY',
        'pgsql' => 'SERIAL NOT NULL PRIMARY KEY',
        'sqlite' => 'INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT',
        'sqlsrv' => 'INTEGER IDENTITY NOT NULL PRIMARY KEY',
    ][DB_DRIVER] ?? '';
    if (empty($pkColDef)) {
        throw new Exception('Unsupported PDO driver ' . DB_DRIVER, 500);
    }

    $hashType = [
        'mysql' => 'BINARY(64)',
        'pgsql' => 'BYTEA',
        'sqlite' => 'BLOB',
        'sqlsrv' => 'BINARY(64)',
    ][DB_DRIVER];

    $db = getDBConnection(true);

    // MySQL character set and collation handling is error-prone.
    // Ensure connection matches database to avoid much pain.
    if (DB_DRIVER === 'mysql') {
        $dbInfo = $db->query(
            <<<'SQL'
SELECT
    @@character_set_client,
    @@collation_connection,
    @@character_set_connection,
    DEFAULT_CHARACTER_SET_NAME,
    DEFAULT_COLLATION_NAME
FROM INFORMATION_SCHEMA.SCHEMATA
WHERE SCHEMA_NAME = DATABASE()
SQL,
        )->fetch(PDO::FETCH_ASSOC);

        $charsetClient = $dbInfo['@@character_set_client'];
        $charsetConn = $dbInfo['@@character_set_connection'];
        $charsetDB = $dbInfo['DEFAULT_CHARACTER_SET_NAME'];
        if ($charsetClient !== $charsetConn or $charsetConn !== $charsetDB) {
            throw new Exception(
                "Client charset ($charsetClient) does not match "
                . "connection ($charsetConn) and database ($charsetDB)",
                500,
            );
        }

        $collConn = $dbInfo['@@collation_connection'];
        $collDB = $dbInfo['DEFAULT_COLLATION_NAME'];
        if ($collConn !== $collDB) {
            throw new Exception(
                "Connection collation ($collConn) does not match "
                . "database ($collDB)",
                500,
            );
        }
    }

    // MySQL/MariaDB don't support names for column constraints.  Comment out.
    $colConstNameLine = (DB_DRIVER === 'mysql' ? '-- ' : '') . 'CONSTRAINT';

	// phpcs:disable Generic.Files.LineLength.TooLong
    $db->exec(
        <<<SQL
CREATE TABLE IF NOT EXISTS "Players" (
    "TriPeaksPlayerModelID" $pkColDef,
    "Name_Identifier" VARCHAR(64) NOT NULL
        $colConstNameLine UK_Players_Name_Identifier
            UNIQUE
        $colConstNameLine CK_Players_Name_Identifier
            CHECK ("Name_Identifier" <> ''),
    "Name" VARCHAR(64) NOT NULL DEFAULT (''),
    -- Note: Players with both null and '' Email exist
    "Email" VARCHAR(64) NULL DEFAULT (''),
    "Rank" INTEGER NOT NULL DEFAULT (0)
        $colConstNameLine CK_Players_Rank
            CHECK ("Rank" >= 0),
    "Score" FLOAT NOT NULL DEFAULT (0)
        $colConstNameLine CK_Players_Score
            CHECK ("Score" >= 0),
    "Streak" SMALLINT NOT NULL DEFAULT (0)
        $colConstNameLine CK_Players_Streak
            CHECK ("Streak" >= 0),
    "GamesPlayed" INTEGER NOT NULL DEFAULT (0)
        $colConstNameLine CK_Players_GamesPlayed
            CHECK ("GamesPlayed" >= 0),
    "MostWon" INTEGER NOT NULL DEFAULT (0)
        $colConstNameLine CK_Players_MostWon
            CHECK ("MostWon" >= 0),
    "MostLost" INTEGER NOT NULL DEFAULT (0)
        $colConstNameLine CK_Players_MostLost
            CHECK ("MostLost" <= 0),
    "Cleared" INTEGER NOT NULL DEFAULT (0)
        $colConstNameLine CK_Players_Cleared
            CHECK ("Cleared" >= 0),
    "Months" SMALLINT NOT NULL DEFAULT (0)
        $colConstNameLine CK_Players_Months
            CHECK ("Months" >= 0),
    -- Note: A user has Lastdate -238.  All others are 0 < Lastdate <= 238
    "Lastdate" SMALLINT NOT NULL DEFAULT (0),
    "Status" SMALLINT NOT NULL DEFAULT (0),
    "Registration" VARCHAR(64) NOT NULL DEFAULT ('Free'),
    "Tokens" SMALLINT NOT NULL DEFAULT (0)
        $colConstNameLine CK_Players_Tokens
            CHECK ("Tokens" >= 0),
    "Version" SMALLINT NOT NULL DEFAULT (3),
    "Relationship" SMALLINT NOT NULL DEFAULT (0),
    -- Note: Players with both null and '' Res1 exist
    "Res1" VARCHAR(16) NULL DEFAULT ('                '),
    -- Note: Players with both null and '' Res2 exist
    "Res2" VARCHAR(16) NULL DEFAULT ('                '),

    "LoweredName_Identifier" VARCHAR(64) NOT NULL
        $colConstNameLine UK_Players_LoweredName_Identifier
            UNIQUE
        $colConstNameLine CK_Players_LoweredName_Identifier
            CHECK ("LoweredName_Identifier" NOT LIKE ''
                AND "LoweredName_Identifier" NOT LIKE '%[A-Z]%'
                AND "LoweredName_Identifier" NOT LIKE '% '),
    "LoweredName" VARCHAR(64) NOT NULL
        $colConstNameLine CK_Players_LoweredName
            CHECK ("LoweredName" NOT LIKE '%[A-Z]%'
                AND "LoweredName" NOT LIKE '% '),
    "HashedPassword" $hashType NOT NULL
)
SQL,
    );
	// phpcs:enable

    // Note: I would have made (LoweredName, HashedPassword) UNIQUE, but
    // TriPeaks API didn't.  GetPlayerByNamePassword can have multiple results.
    // Order by TriPeaksPlayerModelID for determinism in testing.
    $db->exec(
        <<<SQL
CREATE INDEX IF NOT EXISTS IX_Players_LoweredName_PasswordHash ON "Players" (
    "LoweredName",
    "HashedPassword",
    "TriPeaksPlayerModelID"
)
SQL,
    );

    // Note: Unclear how TriPeaks orders players with same Score.
    // Use LoweredName_Identifier to make deterministic for our own testing
    $db->exec(
        <<<SQL
CREATE INDEX IF NOT EXISTS IX_Players_Score ON "Players" (
    "Score",
    "LoweredName_Identifier"
)
SQL,
    );
}


/**
 * Saves updated (or new) player information.
 *
 * @param array $player Updated Player.
 * @param PDO   $db     DB connection.
 *
 * @return integer Number of rows updated (always 1).
 */
function upsertPlayer(array $player, PDO $db) : int
{
    // INSERT can only be done if Name and Password/HashedPassword are given
    // Otherwise, INSERT will raise an error before ON DUPLICATE is considered
    $canInsert = true;

    // Only set columns which are present in request.
    $updateColTypes = array_intersect_key(PLAYER_COLUMNS_UPDATE, $player);
    // Name_Identifier is used for lookup, never changes.
    unset($updateColTypes['Name_Identifier']);

    $updateColNames = array_keys($updateColTypes);
    // Set LoweredName along with Name
    if (array_key_exists('Name', $player)) {
        $updateColNames[] = 'LoweredName';
    } else {
        $canInsert = false;
    }

    // Set HashedPassword instead of Password
    if (array_key_exists('Password', $player)
        or array_key_exists('HashedPassword', $player)
    ) {
        $updateColNames[] = 'HashedPassword';
    } else {
        $canInsert = false;
    }

    if (empty($updateColNames)) {
        // Update succeeds trivially.
        // FIXME: Should fail if Name_Identifier doesn't exist.
        return 1;
    }

    $insertColNames = array_merge(
        $updateColNames,
        [
            'Name_Identifier',
            'LoweredName_Identifier',
        ],
    );
    $insertCols = '"' . implode("\",\n    \"", $insertColNames) . '"';

    // Note: Need separate variable names for insert and update since we
    // can't repeat params on mysql non-emulated: https://bugs.php.net/69257
    $insertVarNames = array_map(
        function ($colName) {
            return ":{$colName}Ins";
        },
        $insertColNames,
    );
    $insertVars = implode(",\n    ", $insertVarNames);

    $updateColSets = array_map(
        function ($colName) {
            return "\"$colName\" = :{$colName}Upd";
        },
        $updateColNames,
    );
    $updateCols = implode(",\n    ", $updateColSets);

    $needLoweredNameId = false;
    if (!$canInsert) {
        $needLoweredNameId = true;
        $upsertSql = <<<SQL
UPDATE "Players"
SET $updateCols
WHERE "LoweredName_Identifier" = :LoweredName_Identifier
SQL;
    } else {
        switch (DB_DRIVER) {
        case 'mysql':
            $upsertSql = <<<SQL
INSERT INTO "Players" (
    $insertCols
) VALUES (
    $insertVars
) ON DUPLICATE KEY UPDATE
    $updateCols
SQL;
            break;

        case 'pgsql':
        case 'sqlite':
            $upsertSql = <<<SQL
INSERT INTO "Players" (
    $insertCols
) VALUES (
    $insertVars
) ON CONFLICT ("LoweredName_Identifier") DO UPDATE SET
    $updateCols
SQL;
            break;

        case 'sqlsrv':
            $needLoweredNameId = true;
            $upsertSql = <<<SQL
MERGE INTO "Players"
USING (SELECT :LoweredName_Identifier AS "LoweredName_Identifier") AS source
ON ("Players"."LoweredName_Identifier" = source."LoweredName_Identifier")
WHEN MATCHED THEN UPDATE SET
    $updateCols
WHEN NOT MATCHED THEN INSERT (
    $insertCols
) VALUES (
    $insertVars
)
SQL;
            break;

        default:
            throw new Exception('Unsupported PDO driver ' . DB_DRIVER, 500);
        }
    }

    $upsertStmt = $db->prepare($upsertSql);
    foreach ($updateColTypes as $name => $type) {
        if ($canInsert) {
            $upsertStmt->bindValue(":{$name}Ins", $player[$name], $type);
        }

        $upsertStmt->bindValue(":{$name}Upd", $player[$name], $type);
    }

    $loweredNameIdentifier
        = strtolower(rtrim($player['Name_Identifier'], ' '));
    if ($needLoweredNameId) {
        $upsertStmt->bindValue(
            ':LoweredName_Identifier',
            $loweredNameIdentifier,
            PLAYER_COLUMNS_UPDATE['Name_Identifier'],
        );
    }

    if ($canInsert) {
        $upsertStmt->bindValue(
            ':Name_IdentifierIns',
            $player['Name_Identifier'],
            PLAYER_COLUMNS_UPDATE['Name_Identifier'],
        );
        $upsertStmt->bindValue(
            ':LoweredName_IdentifierIns',
            $loweredNameIdentifier,
            PLAYER_COLUMNS_UPDATE['Name_Identifier'],
        );
    }

    if (array_key_exists('Name', $player)) {
        if ($canInsert) {
            $upsertStmt->bindValue(
                ':LoweredNameIns',
                strtolower(rtrim($player['Name'], ' ')),
                $updateColTypes['Name'],
            );
        }

        $upsertStmt->bindValue(
            ':LoweredNameUpd',
            strtolower(rtrim($player['Name'], ' ')),
            $updateColTypes['Name'],
        );
    }

    if (array_key_exists('Password', $player)
        or array_key_exists('HashedPassword', $player)
    ) {
        if ($canInsert) {
            $upsertStmt->bindValue(
                ':HashedPasswordIns',
                $player['HashedPassword'] ?? hashPassword($player['Password']),
                PDO::PARAM_LOB,
            );
        }

        $upsertStmt->bindValue(
            ':HashedPasswordUpd',
            $player['HashedPassword'] ?? hashPassword($player['Password']),
            PDO::PARAM_LOB,
        );
    }

    $upsertStmt->execute();
    $rowCount = $upsertStmt->rowCount();

    // MariaDB and MySQL return 2 if a row is updated
    // https://mariadb.com/kb/en/library/insert-on-duplicate-key-update/
    // https://dev.mysql.com/doc/refman/8.0/en/insert-on-duplicate.html
    return DB_DRIVER === 'mysql' && $rowCount === 2 ? 1 : $rowCount;
}


/**
 * Saves updated (or new) player information.
 *
 * @param array $body POST body.
 *
 * @return integer Number of rows updated (always 1).
 */
function updatePlayer($body) : int
{
    if (!is_array($body)) {
        throw new Exception('Expected JSON request body to be an object', 400);
    }

    $model = $body['model'] ?? null;
    if (!is_array($model)) {
        throw new Exception('Expected model to be an object', 400);
    }

    $nameIdentifier = $model['Name_Identifier'] ?? null;
    if (!is_string($nameIdentifier)) {
        throw new Exception('Expected model.Name_Identifier to be a string', 400);
    }

    // Don't update password to the placeholder sent in responses,
    // since the TriPeaksLite.xap sends it back in updates.
    if (array_key_exists('Password', $model)
        && $model['Password'] === PASSWORD_PLACEHOLDER
    ) {
        unset($model['Password']);
    }

    // TriPeaksPlayerModelID can't be updated (and is always 0 in request).
    unset($model['TriPeaksPlayerModelID']);

    // HashedPassword shouldn't be present in request
    unset($model['HashedPassword']);

    $db = getDBConnection(true);
    return upsertPlayer($model, $db);
}


/**
 * Dispatches a request to the appropriate request handler.
 *
 * @param array $server              $_SERVER
 * @param array $post                $_POST
 * @param array $handlerByPathMethod Map path then method to handler function.
 *
 * @return mixed Response body data.
 */
function dispatch($server, $post, $handlerByPathMethod)
{
    $method = $server['REQUEST_METHOD'];

    // Although methanegames.com supports Transfer-Encoding: chunked,
    // Apache+PHP does not (See https://bugs.php.net/51191)
    // So we ignore T-E and send 411 whenever Content-Length is missing
    if (($method === 'POST' or $method === 'PUT')
        and ($server['CONTENT_LENGTH'] ?? null) === null
    ) {
        throw new Exception(
            'The request must be chunked or have a content length.',
            411
        );
    }

    if (!empty($server['PATH_INFO'])) {
        $path = $server['PATH_INFO'];
    } elseif (!empty($server['ORIG_PATH_INFO'])) {
        $path = $server['ORIG_PATH_INFO'];
    } else {
        $path = '';
    }

    if (!isset($handlerByPathMethod[$path])) {
        throw new Exception("Unrecognized api path \"$path\"", 404);
    }

    $methodHandlers = $handlerByPathMethod[$path];
    if (empty($methodHandlers[$method])) {
        $methodsStr = implode(', ', array_keys($methodHandlers));
        header("Allow: $methodsStr");
        throw new Exception(
            "The requested resource does not support http method '$method'.",
            405
        );
    }

    if (!isJson($server['CONTENT_TYPE'] ?? '')) {
        // methanegames.com returns JSON response with null value
        return null;
    }

    $handlerName = $methodHandlers[$method];
    $body = json_decode(file_get_contents('php://input'), true);
    return call_user_func($handlerName, $body);
}


if (!defined('TRIPEAKS_NO_OUTPUT')) {
    /* Headers sent by methanegames.com:
     * header('Server: Microsoft-IIS/8.0');
     * header('X-AspNet-Version: 4.0.30319');
     * header('X-Powered-By: ASP.NET');
     */

    try {
        $result = dispatch($_SERVER, $_POST, API_PATH_METHOD_HANDLER);
        $json = json_encode(
            $result,
            JSON_THROW_ON_ERROR
                | JSON_PRESERVE_ZERO_FRACTION
                | JSON_UNESCAPED_LINE_TERMINATORS
                | JSON_UNESCAPED_SLASHES
                | JSON_UNESCAPED_UNICODE,
        );
    } catch (Exception $ex) {
        $code = $ex->getCode();
        if (empty($code) or $code < 400 or $code >= 600) {
            $code = 500;
        }

        if (!headers_sent()) {
            http_response_code($code);
        }

        switch ($code) {
        case 404:
            // Match headers sent by methanegames.com
            header('Cache-Control: private');
            header('Content-Type: text/html; charset=utf-8');
            include __DIR__ . DIRECTORY_SEPARATOR . '404.php';
            return;
        case 411:
            header('Content-Type: text/html; charset=us-ascii');
            $filename = __DIR__ . DIRECTORY_SEPARATOR . '411.html';
            header('Content-Length: ' . filesize($filename));
            // header('Server: Microsoft-HTTPAPI/2.0');
            readfile($filename);
            return;
        }

        $json = json_encode(
            [
                'Message' => $ex->getMessage(),
            ],
        );
    }

    // Match headers sent by methanegames.com
    header('Cache-Control: no-cache');
    header('Pragma: no-cache');
    header('Content-Type: application/json; charset=utf-8');
    header('Content-Length: ' . strlen($json));
    header('Expires: -1');

    echo $json;
}


// vim: set sts=4 sw=4 et :
