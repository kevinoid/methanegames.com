<?php
/**
 * Integration tests for the TriPeaksLite API reimplementation.
 *
 * PHP Version 7.3
 *
 * @author  Kevin Locke <kevin@kevinlocke.name>
 * @license CC0 https://creativecommons.org/publicdomain/zero/1.0/
 */

declare(strict_types=1);

// This script is only intended to be used for automated testing.
// It wipes the database and therefore should not be run in production.
if (PHP_SAPI !== 'cli') {
    http_response_code(403);
    header('Content-Type: text/plain');
    die('This file can only be run by php-cli.');
}

// Prevent TriPeaks.php from rendering a response
const TRIPEAKS_NO_OUTPUT = true;

require __DIR__ . DIRECTORY_SEPARATOR . 'TriPeaks.php';


// phpcs:disable PEAR.Commenting


const PLAYER1 = [
    'TriPeaksPlayerModelID' => 26396,
    'Name_Identifier' => 'Kevin637054460681742606    ',
    'Name' => 'Kevin',
    'Email' => '',
    'Rank' => 0,
    'Score' => 0.0,
    'Streak' => 0,
    'GamesPlayed' => 1,
    'MostWon' => 0,
    'MostLost' => -140,
    'Cleared' => 0,
    'Months' => 237,
    'Lastdate' => 237,
    'Password' => PASSWORD_PLACEHOLDER,
    'Status' => 0,
    'Registration' => 'Yes',
    'Tokens' => 5,
    'Version' => 1,
    'Relationship' => 0,
    'Res1' => '',
    'Res2' => '',
];

const EMPTY_NAME_ID = 'Kevin637054460681742607    ';
const SAME_NAME_ID = 'Devin637054460681742606    ';
const UNICODE_NAME = "\u{1F600}";

// GetPlayerByNameID request with observed property order and values
const REQUEST_GET_PLAYER1_BY_NAME_ID = [
    'model' => [
        'Cleared' => 0,
        'Email' => null,
        'GamesPlayed' => 0,
        'Lastdate' => 0,
        'Months' => 0,
        'MostLost' => 0,
        'MostWon' => 0,
        'Name' => null,
        'Name_Identifier' => 'Kevin637054460681742606    ',
        'Password' => null,
        'Rank' => 0,
        'Registration' => null,
        'Relationship' => 0,
        'Res1' => null,
        'Res2' => null,
        'Score' => 0,
        'Status' => 0,
        'Streak' => 0,
        'Tokens' => 0,
        'TriPeaksPlayerModelID' => 0,
        'Version' => 0,
    ],
    'name' => null,
    'range' => 0,
    'requestId' => null,
    'score' => 0,
];
// GetPlayerByNamePassword request with observed property order and values
const REQUEST_GET_PLAYER1_BY_NAME_PASSWORD = [
    'model' => [
        'Cleared' => 0,
        'Email' => null,
        'GamesPlayed' => 0,
        'Lastdate' => 0,
        'Months' => 0,
        'MostLost' => 0,
        'MostWon' => 0,
        'Name' => 'Kevin',
        'Name_Identifier' => null,
        'Password' => 'Kevin## ',
        'Rank' => 0,
        'Registration' => null,
        'Relationship' => 0,
        'Res1' => null,
        'Res2' => null,
        'Score' => 0,
        'Status' => 0,
        'Streak' => 0,
        'Tokens' => 0,
        'TriPeaksPlayerModelID' => 0,
        'Version' => 0,
    ],
    'name' => null,
    'range' => 0,
    'requestId' => null,
    'score' => 0,
];
// GetPlayerRangeByName request with observed property order and values
const REQUEST_GET_PLAYER1_RANGE_BY_NAME = [
    'model' => [
        'Cleared' => 0,
        'Email' => '',
        'GamesPlayed' => 1,
        'Lastdate' => 237,
        'Months' => 237,
        'MostLost' => -140,
        'MostWon' => 0,
        'Name' => 'Kevin',
        'Name_Identifier' => 'Kevin637054460681742606    ',
        'Password' => PASSWORD_PLACEHOLDER,
        'Rank' => 0,
        'Registration' => 'Yes',
        'Relationship' => 0,
        'Res1' => '',
        'Res2' => '',
        'Score' => 0.0,
        'Status' => 0,
        'Streak' => 0,
        'Tokens' => 5,
        'TriPeaksPlayerModelID' => 0,
        'Version' => 1,
    ],
    'name' => null,
    'range' => 10,
    'requestId' => null,
    'score' => 0,
];
// GetPlayerRangeByScore request with observed property order and values
const REQUEST_GET_PLAYER_RANGE_BY_SCORE = [
    'model' => null,
    'name' => null,
    'range' => 10,
    'requestId' => null,
    'score' => 200,
];
// GetPlayerRangeFromTop request with observed property order and values
const REQUEST_GET_PLAYER_RANGE_FROM_TOP = [
    'model' => null,
    'name' => null,
    'range' => 10,
    'requestId' => null,
    'score' => 0,
];
// GetScoreRank request with observed property order and values
const REQUEST_GET_PLAYER1_SCORE_RANK = [
    'model' => [
        'Cleared' => 0,
        'Email' => '',
        'GamesPlayed' => 1,
        'Lastdate' => 237,
        'Months' => 237,
        'MostLost' => -140,
        'MostWon' => 0,
        'Name' => 'Kevin',
        'Name_Identifier' => 'Kevin637054460681742606    ',
        'Password' => PASSWORD_PLACEHOLDER,
        'Rank' => 0,
        'Registration' => 'Yes',
        'Relationship' => 0,
        'Res1' => '',
        'Res2' => '',
        'Score' => 0.0,
        'Status' => 0,
        'Streak' => 0,
        'Tokens' => 5,
        'TriPeaksPlayerModelID' => 26396,
        'Version' => 1,
    ],
    'name' => null,
    'range' => 10,
    'requestId' => null,
    'score' => 0,
];
// UpdatePlayer request with observed property order and values
const REQUEST_UPDATE_PLAYER1 = [
    'model' => [
        'Cleared' => 0,
        'Email' => '',
        'GamesPlayed' => 1,
        'Lastdate' => 237,
        'Months' => 237,
        'MostLost' => -140,
        'MostWon' => 0,
        'Name' => 'Kevin',
        'Name_Identifier' => 'Kevin637054460681742606    ',
        'Password' => 'Kevin## ',
        'Rank' => 0,
        'Registration' => 'Yes',
        'Relationship' => 0,
        'Res1' => '',
        'Res2' => '',
        'Score' => 0,
        'Status' => 0,
        'Streak' => 0,
        'Tokens' => 5,
        'TriPeaksPlayerModelID' => 0,
        'Version' => 1,
    ],
    'name' => null,
    'range' => 0,
    'requestId' => null,
    'score' => 0,
];


function assertEquals($expected, $actual, ?string $message=null)
{
    if ($expected !== $actual) {
        $ex = new Exception($message ?? 'Expected arguments to be identical.');
        $ex->expected = $expected;
        $ex->actual = $actual;
        throw $ex;
    }
}


/**
 * Tests for TriPeaks (in PHPUnit style).
 */
final class TriPeaksTest
{
    /**
     * Players created for testing.
     *
     * @var array
     */
    private $testPlayers = [];


    private static function comparePlayersByScoreID(
        array $player1,
        array $player2
    ) : int {
        $cmp = $player1['Score'] <=> $player2['Score'];
        if ($cmp !== 0) {
            return $cmp;
        }

        return $player1['Name_Identifier'] <=> $player2['Name_Identifier'];
    }


    public function testWipeDB()
    {
        wipeDB();
    }


    public function testInitializeDB()
    {
        initializeDB();
    }


    public function testGetPlayerByNameIDNonexistent()
    {
        $result = getPlayerByNameID(REQUEST_GET_PLAYER1_BY_NAME_ID);
        assertEquals(null, $result);
    }


    public function testGetPlayerByNamePasswordNonexistent()
    {
        $result = getPlayerByNamePassword(REQUEST_GET_PLAYER1_BY_NAME_PASSWORD);
        assertEquals(null, $result);
    }


    // Note: No reference comparison for empty DB
    public function testGetPlayerRangeByNameNonexistent()
    {
        $result = getPlayerRangeByName(REQUEST_GET_PLAYER1_RANGE_BY_NAME);
        assertEquals(null, $result);
    }


    // Note: No reference comparison for empty DB
    public function testGetPlayerRangeByScoreNonexistent()
    {
        $result = getPlayerRangeByScore(REQUEST_GET_PLAYER_RANGE_BY_SCORE);
        assertEquals(null, $result);
    }


    // Note: No reference comparison for empty DB
    public function testGetPlayerRangeFromTopNonexistent()
    {
        $result = getPlayerRangeFromTop(REQUEST_GET_PLAYER_RANGE_FROM_TOP);
        assertEquals(null, $result);
    }


    // Note: No reference comparison for empty DB
    public function testGetScoreRankNonexistent()
    {
        $result = getScoreRank(REQUEST_GET_PLAYER1_SCORE_RANK);
        assertEquals(0, $result);
    }


    public function testUpdatePlayerCreatesPlayer()
    {
        $result = updatePlayer(REQUEST_UPDATE_PLAYER1);
        assertEquals(1, $result);
    }


    public function testGetPlayerByNameIDExact()
    {
        $result = getPlayerByNameID(REQUEST_GET_PLAYER1_BY_NAME_ID);

        // TriPeaksPlayerModelID can be any server-generated integer.
        $player = PLAYER1;
        $player['TriPeaksPlayerModelID']
            = $result[0]['TriPeaksPlayerModelID'] ?? 0;

        assertEquals([$player], $result);
    }


    public function testGetPlayerByNameIDLowerTrimmed()
    {
        $request = REQUEST_GET_PLAYER1_BY_NAME_ID;
        $request['model']['Name_Identifier']
            = strtolower(rtrim($request['model']['Name_Identifier'], ' '));
        $result = getPlayerByNameID($request);

        // TriPeaksPlayerModelID can be any server-generated integer.
        $player = PLAYER1;
        $player['TriPeaksPlayerModelID']
            = $result[0]['TriPeaksPlayerModelID'] ?? 0;

        assertEquals([$player], $result);
    }


    public function testGetPlayerByNameIDUpperPadded()
    {
        $request = REQUEST_GET_PLAYER1_BY_NAME_ID;
        $request['model']['Name_Identifier']
            = strtoupper($request['model']['Name_Identifier']) . ' ';
        $result = getPlayerByNameID($request);

        // TriPeaksPlayerModelID can be any server-generated integer.
        $player = PLAYER1;
        $player['TriPeaksPlayerModelID']
            = $result[0]['TriPeaksPlayerModelID'] ?? 0;

        assertEquals([$player], $result);
    }


    public function testGetPlayerByNamePasswordExact()
    {
        $result = getPlayerByNamePassword(REQUEST_GET_PLAYER1_BY_NAME_PASSWORD);

        // TriPeaksPlayerModelID can be any server-generated integer.
        $player = PLAYER1;
        $player['TriPeaksPlayerModelID']
            = $result[0]['TriPeaksPlayerModelID'] ?? 0;

        assertEquals([$player], $result);
    }


    public function testGetPlayerByNamePasswordLowerTrimmed()
    {
        $request = REQUEST_GET_PLAYER1_BY_NAME_PASSWORD;
        $request['model']['Password']
            = strtolower(rtrim($request['model']['Password'], ' '));
        $result = getPlayerByNamePassword($request);

        // TriPeaksPlayerModelID can be any server-generated integer.
        $player = PLAYER1;
        $player['TriPeaksPlayerModelID']
            = $result[0]['TriPeaksPlayerModelID'] ?? 0;

        assertEquals([$player], $result);
    }


    public function testGetPlayerByNamePasswordUpperPadded()
    {
        $request = REQUEST_GET_PLAYER1_BY_NAME_PASSWORD;
        $request['model']['Name_Identifier']
            = strtoupper($request['model']['Password']) . ' ';
        $result = getPlayerByNamePassword($request);

        // TriPeaksPlayerModelID can be any server-generated integer.
        $player = PLAYER1;
        $player['TriPeaksPlayerModelID']
            = $result[0]['TriPeaksPlayerModelID'] ?? 0;

        assertEquals([$player], $result);
    }


    // Note: No reference comparison for < $range in DB
    public function testGetPlayerRangeByName()
    {
        $result = getPlayerRangeByName(REQUEST_GET_PLAYER1_RANGE_BY_NAME);

        // TriPeaksPlayerModelID can be any server-generated integer.
        $player = PLAYER1;
        $player['TriPeaksPlayerModelID']
            = $result[0]['TriPeaksPlayerModelID'] ?? 0;

        assertEquals([$player], $result);
    }


    // Note: No reference comparison for < $range in DB
    public function testGetPlayerRangeByScore()
    {
        $result = getPlayerRangeByScore(REQUEST_GET_PLAYER_RANGE_BY_SCORE);

        // TriPeaksPlayerModelID can be any server-generated integer.
        $player = PLAYER1;
        $player['TriPeaksPlayerModelID']
            = $result[0]['TriPeaksPlayerModelID'] ?? 0;

        assertEquals([$player], $result);
    }


    // Note: No reference comparison for < $range in DB
    public function testGetPlayerRangeFromTop()
    {
        $result = getPlayerRangeFromTop(REQUEST_GET_PLAYER_RANGE_FROM_TOP);

        // TriPeaksPlayerModelID can be any server-generated integer.
        $player = PLAYER1;
        $player['TriPeaksPlayerModelID']
            = $result[0]['TriPeaksPlayerModelID'] ?? 0;

        assertEquals([$player], $result);
    }


    public function testGetScoreRank()
    {
        $result = getScoreRank(REQUEST_GET_PLAYER1_SCORE_RANK);
        // Top player has rank 0
        assertEquals(0, $result);
    }


    public function testCreateMultiplePlayers()
    {
        $getRequest = REQUEST_GET_PLAYER1_BY_NAME_ID;
        $player = getPlayerByNameID($getRequest);
        assertEquals(1, count($player));
        $this->testPlayers[] = $player[0];

        $updateRequest = REQUEST_UPDATE_PLAYER1;
        for ($i = 0; $i < 15; ++$i) {
            $id = (string) mt_rand(100000000, 999999999);
            // Every 4th player has a duplicate score
            if ($i % 4 !== 0 or $i === 0) {
                $score = mt_rand(0, 1000) / 10;
            }

            // Note: 0 at end is used to ensure prefix is unique in
            // testGetPlayerRangeByNameMatchesPrefix
            $updateRequest['model']['Name_Identifier'] = "Kevin{$id}0";
            $updateRequest['model']['Password'] = $id;
            $updateRequest['model']['Score'] = $score;
            $result = updatePlayer($updateRequest);
            assertEquals(1, $result);

            $getRequest['model']['Name_Identifier']
                = $updateRequest['model']['Name_Identifier'];
            $player = getPlayerByNameID($getRequest);
            assertEquals(1, count($player));
            $this->testPlayers[] = $player[0];
        }

        usort($this->testPlayers, 'TriPeaksTest::comparePlayersByScoreID');
        $this->testPlayers = array_reverse($this->testPlayers);
    }


    public function testGetPlayerRangeByNameTop()
    {
        $request = REQUEST_GET_PLAYER1_RANGE_BY_NAME;
        $request['model']['Name_Identifier']
            = $this->testPlayers[0]['Name_Identifier'];
        $result = getPlayerRangeByName($request);

        $players = array_slice($this->testPlayers, 0, $request['range']);
        assertEquals($players, $result);
    }


    public function testGetPlayerRangeByNameMiddle()
    {
        $i = count($this->testPlayers) >> 1;

        $request = REQUEST_GET_PLAYER1_RANGE_BY_NAME;
        $request['model']['Name_Identifier']
            = $this->testPlayers[$i]['Name_Identifier'];
        $result = getPlayerRangeByName($request);

        $range = $request['range'];
        $players = array_slice($this->testPlayers, $i - ($range >> 1), $range);
        assertEquals($players, $result);
    }


    public function testGetPlayerRangeByNameMatchesPrefix()
    {
        $i = count($this->testPlayers) >> 1;

        $request = REQUEST_GET_PLAYER1_RANGE_BY_NAME;
        $request['model']['Name_Identifier']
            = substr($this->testPlayers[$i]['Name_Identifier'], 0, -1);
        $result = getPlayerRangeByName($request);

        $range = $request['range'];
        $players = array_slice($this->testPlayers, $i - ($range >> 1), $range);
        assertEquals($players, $result);
    }


    public function testGetPlayerRangeByNameCaseSensitive()
    {
        $request = REQUEST_GET_PLAYER1_RANGE_BY_NAME;
        $request['model']['Name_Identifier']
            = strtoupper($this->testPlayers[0]['Name_Identifier']);
        $result = getPlayerRangeByName($request);

        assertEquals(null, $result);
    }


    public function testGetPlayerRangeByNamePaddingSensitive()
    {
        $request = REQUEST_GET_PLAYER1_RANGE_BY_NAME;
        $request['model']['Name_Identifier']
            = $this->testPlayers[0]['Name_Identifier'] . ' ';
        $result = getPlayerRangeByName($request);

        assertEquals(null, $result);
    }


    // Note: No reference comparison to query last (since many have Score 0)
    public function testGetPlayerRangeByNameBottom()
    {
        $request = REQUEST_GET_PLAYER1_RANGE_BY_NAME;
        $request['model']['Name_Identifier']
            = $this->testPlayers[count($this->testPlayers) - 1]['Name_Identifier'];
        $result = getPlayerRangeByName($request);

        $count = (($request['range'] + 1) >> 1) + 1;
        $players = array_slice($this->testPlayers, -$count);
        assertEquals($players, $result);
    }


    public function testGetPlayerRangeByScoreTop()
    {
        $request = REQUEST_GET_PLAYER_RANGE_BY_SCORE;
        $request['score'] = $this->testPlayers[0]['Score'] + 1;
        $result = getPlayerRangeByScore($request);

        $players = array_slice($this->testPlayers, 0, $request['range']);
        assertEquals($players, $result);
    }


    public function testGetPlayerRangeByScoreMiddle()
    {
        // First player with score ~1/4 down list
        $i = count($this->testPlayers);
        $i = $i - ($i >> 2);
        $score = $this->testPlayers[$i]['Score'];
        while ($i > 0 and $this->testPlayers[$i - 1]['Score'] === $score) {
            --$i;
        }

        $request = REQUEST_GET_PLAYER_RANGE_BY_SCORE;
        $request['score'] = $score;
        $result = getPlayerRangeByScore($request);

        $players = array_slice($this->testPlayers, $i, $request['range']);
        assertEquals($players, $result);
    }


    public function testGetPlayerRangeByScoreBottom()
    {
        // First player with lowest score
        $i = count($this->testPlayers) - 1;
        $score = $this->testPlayers[$i]['Score'];
        while ($i > 0 and $this->testPlayers[$i - 1]['Score'] === $score) {
            --$i;
        }

        $request = REQUEST_GET_PLAYER_RANGE_BY_SCORE;
        $request['score'] = $score;
        $result = getPlayerRangeByScore($request);

        $players = array_slice($this->testPlayers, $i);
        assertEquals($players, $result);
    }


    public function testGetPlayerRangeFromTop2()
    {
        $request = REQUEST_GET_PLAYER_RANGE_FROM_TOP;
        $result = getPlayerRangeFromTop($request);

        $players = array_slice($this->testPlayers, 0, $request['range']);
        assertEquals($players, $result);
    }


    public function testGetScoreRankTop()
    {
        $request = REQUEST_GET_PLAYER1_SCORE_RANK;
        $request['model']['Name_Identifier']
            = $this->testPlayers[0]['Name_Identifier'];
        $result = getScoreRank($request);

        // Top player has rank 0
        assertEquals(0, $result);
    }


    public function testGetScoreRankMiddle()
    {
        // Find a rank in the middle with same scores
        $rank = count($this->testPlayers) >> 2;
        while ($this->testPlayers[$rank]['Score']
                !== $this->testPlayers[$rank]['Score']) {
            ++$rank;
        }

        $request = REQUEST_GET_PLAYER1_SCORE_RANK;
        $request['model']['Name_Identifier']
            = $this->testPlayers[$rank]['Name_Identifier'];
        $result = getScoreRank($request);
        assertEquals($rank, $result);

        $request['model']['Name_Identifier']
            = $this->testPlayers[$rank + 1]['Name_Identifier'];
        $result = getScoreRank($request);
        assertEquals($rank + 1, $result);
    }


    public function testGetScoreRankBottom()
    {
        $rank = count($this->testPlayers) - 1;
        $request = REQUEST_GET_PLAYER1_SCORE_RANK;
        $request['model']['Name_Identifier']
            = $this->testPlayers[$rank]['Name_Identifier'];
        $result = getScoreRank($request);

        assertEquals($rank, $result);
    }


    public function testGetScoreRankBottomNoMatch()
    {
        $request = REQUEST_GET_PLAYER1_SCORE_RANK;
        $request['model']['Name_Identifier'] = 'nonexistent';
        $result = getScoreRank($request);

        assertEquals(count($this->testPlayers), $result);
    }


    public function testCreateSameNamePassword()
    {
        $request = REQUEST_UPDATE_PLAYER1;
        $request['model']['Name_Identifier'] = SAME_NAME_ID;
        $result = updatePlayer($request);
        assertEquals(1, $result);
    }


    public function testGetPlayerByNamePasswordMultiple()
    {
        $result
            = getPlayerByNamePassword(REQUEST_GET_PLAYER1_BY_NAME_PASSWORD);

        // TriPeaksPlayerModelID can be any server-generated integer.
        $player1 = PLAYER1;
        $player1['TriPeaksPlayerModelID']
            = $result[0]['TriPeaksPlayerModelID'] ?? 0;

        $player2 = PLAYER1;
        $player2['TriPeaksPlayerModelID']
            = $result[1]['TriPeaksPlayerModelID'] ?? 0;
        $player2['Name_Identifier'] = SAME_NAME_ID;

        assertEquals([$player1, $player2], $result);
    }


    // Rename the account so it doesn't complicate subsequent tests
    public function testUndoSameNamePassword()
    {
        $request = REQUEST_UPDATE_PLAYER1;
        $request['model']['Name_Identifier'] = SAME_NAME_ID;
        $request['model']['Name'] .= '2';
        $result = updatePlayer($request);
        assertEquals(1, $result);
    }


    public function testCreateEmptyName()
    {
        $request = REQUEST_UPDATE_PLAYER1;
        $request['model']['Name'] = '';
        $request['model']['Name_Identifier'] = EMPTY_NAME_ID;

        // Deviation from TriPeaksLite:
        // Placeholder password is ignored (tested below) so player creation
        // would fail.  Only test blank password if not using it as placeholder.
        //
        // Note: TriPeaksLite.xap prevents creating with blank password, so
        // this is not expected to be a real issue.
        if (PASSWORD_PLACEHOLDER !== '') {
            $request['model']['Password'] = '';
        }

        $result = updatePlayer($request);
        assertEquals(1, $result);
    }


    public function testGetPlayerByNamePasswordEmpty()
    {
        $request = REQUEST_GET_PLAYER1_BY_NAME_PASSWORD;
        $request['model']['Name'] = '';
        if (PASSWORD_PLACEHOLDER !== '') {
            $request['model']['Password'] = '';
        }

        $result = getPlayerByNamePassword($request);

        // TriPeaksPlayerModelID can be any server-generated integer.
        $player = PLAYER1;
        $player['TriPeaksPlayerModelID']
            = $result[0]['TriPeaksPlayerModelID'] ?? 0;
        $player['Name'] = '';
        $player['Name_Identifier'] = EMPTY_NAME_ID;

        assertEquals([$player], $result);
    }


    public function testCreateUnicode()
    {
        $request = REQUEST_UPDATE_PLAYER1;
        $request['model']['Name'] = UNICODE_NAME;
        $request['model']['Name_Identifier'] = UNICODE_NAME;
        $request['model']['Password'] = UNICODE_NAME;
        $result = updatePlayer($request);
        assertEquals(1, $result);
    }


    public function testGetPlayerByNamePasswordUnicode()
    {
        $request = REQUEST_GET_PLAYER1_BY_NAME_PASSWORD;
        $request['model']['Name'] = UNICODE_NAME;
        $request['model']['Password'] = UNICODE_NAME;
        $result = getPlayerByNamePassword($request);

        // TriPeaksPlayerModelID can be any server-generated integer.
        $player = PLAYER1;
        $player['TriPeaksPlayerModelID']
            = $result[0]['TriPeaksPlayerModelID'] ?? 0;
        $player['Name'] = UNICODE_NAME;
        $player['Name_Identifier'] = UNICODE_NAME;

        assertEquals([$player], $result);
    }


    public function testGetPlayerByNameIDUnicode()
    {
        $request = REQUEST_GET_PLAYER1_BY_NAME_ID;
        $request['model']['Name_Identifier'] = UNICODE_NAME;
        $result = getPlayerByNameID($request);

        // TriPeaksPlayerModelID can be any server-generated integer.
        $player = PLAYER1;
        $player['TriPeaksPlayerModelID']
            = $result[0]['TriPeaksPlayerModelID'] ?? 0;
        $player['Name'] = UNICODE_NAME;
        $player['Name_Identifier'] = UNICODE_NAME;

        assertEquals([$player], $result);
    }


    // Note: This differs from TriPeaksLite API, but is necessary because
    // TriPeaksLite.xap sends placeholder password in updates.
    public function testUpdatePlayerIgnoresPlaceholderPassword()
    {
        $updateRequest = REQUEST_UPDATE_PLAYER1;
        $updateRequest['model']['Password'] = PASSWORD_PLACEHOLDER;
        $updateResult = updatePlayer($updateRequest);
        assertEquals(1, $updateResult);

        $getByIDResult = getPlayerByNameID(REQUEST_GET_PLAYER1_BY_NAME_ID);

        // TriPeaksPlayerModelID can be any server-generated integer.
        $player = PLAYER1;
        $player['TriPeaksPlayerModelID']
            = $getByIDResult[0]['TriPeaksPlayerModelID'] ?? 0;

        assertEquals([$player], $getByIDResult);

        $getByPwResult
            = getPlayerByNamePassword(REQUEST_GET_PLAYER1_BY_NAME_PASSWORD);
        assertEquals([$player], $getByPwResult);
    }


    // Note: TriPeaksLite.xap has not been observed to send requests which
    // lack model properties.  However, this behavior can be very useful for
    // testing and manual changes.
    public function testUpdatePlayerNoProps()
    {
        $updateRequest = [
            'model' => [
                'Name_Identifier' =>
                    REQUEST_UPDATE_PLAYER1['model']['Name_Identifier'],
            ],
        ];
        $updateResult = updatePlayer($updateRequest);
        assertEquals(1, $updateResult);

        $getByIDResult = getPlayerByNameID(REQUEST_GET_PLAYER1_BY_NAME_ID);

        // TriPeaksPlayerModelID can be any server-generated integer.
        $player = PLAYER1;
        $player['TriPeaksPlayerModelID']
            = $getByIDResult[0]['TriPeaksPlayerModelID'] ?? 0;

        assertEquals([$player], $getByIDResult);

        $getByPwResult
            = getPlayerByNamePassword(REQUEST_GET_PLAYER1_BY_NAME_PASSWORD);
        assertEquals([$player], $getByPwResult);
    }


    // Note: TriPeaksLite.xap has not been observed to send requests which
    // lack model properties.  However, this behavior can be very useful for
    // testing and manual changes.
    public function testUpdatePlayerPartial()
    {
        // 0-30 random printable ASCII chars
        $password = preg_replace('/[^\x20-\x7E]+/', '', random_bytes(30));
        $updateRequest = [
            'model' => [
                'Name' =>
                    REQUEST_UPDATE_PLAYER1['model']['Name'],
                'Name_Identifier' =>
                    REQUEST_UPDATE_PLAYER1['model']['Name_Identifier'],
                'Password' => $password,
            ],
        ];
        $updateResult = updatePlayer($updateRequest);
        assertEquals(1, $updateResult);

        $getByIDResult = getPlayerByNameID(REQUEST_GET_PLAYER1_BY_NAME_ID);

        // TriPeaksPlayerModelID can be any server-generated integer.
        $player = PLAYER1;
        $player['TriPeaksPlayerModelID']
            = $getByIDResult[0]['TriPeaksPlayerModelID'] ?? 0;

        assertEquals([$player], $getByIDResult);

        $requestByPw = REQUEST_GET_PLAYER1_BY_NAME_PASSWORD;
        $requestByPw['model']['Password'] = $password;
        $getByPwResult = getPlayerByNamePassword($requestByPw);

        assertEquals([$player], $getByPwResult, "Password $password failed.");
    }


    private static function incrementAll(
        array &$player,
        bool $incPassword=true
    ) : void {
        foreach ($player as $key => &$value) {
            if ($key === 'Name_Identifier') {
                continue;
            }

            if (!$incPassword && $key === 'Password') {
                continue;
            }

            switch (gettype($value)) {
            case 'boolean':
                $value = !$value;
                break;
            case 'integer':
            case 'double':
                $value += 1;
                break;
            case 'string':
                $value .= 'X';
                break;
            }
        }
    }


    public function testUpdatePlayerUpdatesPlayer()
    {
        $updateRequest = REQUEST_UPDATE_PLAYER1;
        self::incrementAll($updateRequest['model']);
        $updateResult = updatePlayer($updateRequest);
        assertEquals(1, $updateResult);

        $getByIDResult = getPlayerByNameID(REQUEST_GET_PLAYER1_BY_NAME_ID);

        // TriPeaksPlayerModelID can be any server-generated integer.
        $player = PLAYER1;
        self::incrementAll($player, false);
        $player['TriPeaksPlayerModelID']
            = $getByIDResult[0]['TriPeaksPlayerModelID'] ?? 0;

        assertEquals([$player], $getByIDResult);

        $requestByPw = REQUEST_GET_PLAYER1_BY_NAME_PASSWORD;
        $requestByPw['model']['Name']
            = $updateRequest['model']['Name'];
        $requestByPw['model']['Password']
            = $updateRequest['model']['Password'];
        $getByPwResult = getPlayerByNamePassword($requestByPw);

        assertEquals([$player], $getByPwResult);
    }


}


$testCase = new TriPeaksTest();
$testNames = array_filter(
    get_class_methods($testCase),
    function ($methodName) {
        return substr_compare($methodName, 'test', 0, 4) === 0;
    },
);

header('Content-Type: text/plain');
echo "TAP version 13\n";
echo '1..' . count($testNames) . "\n";
foreach ($testNames as $i => $testName) {
    $tapLine = 'ok ' . ($i + 1) . ' ' . $testName . "\n";
    try {
        $testCase->$testName();
        echo $tapLine;
    } catch (Throwable $ex) {
        echo "not $tapLine";

        if (property_exists($ex, 'actual')) {
            // Use same schema as examples in TAP 13 spec
            // https://testanything.org/tap-version-13-specification.html#yaml-blocks
            $info = [
                'message' => $ex->getMessage(),
                'data' => [
                    'got' => $ex->actual,
                    'expected' => $ex->expected,
                ],
                'stack' => $ex->getTrace(),
            ];
            if (extension_loaded('gd')) {
                $infoStr = yaml_emit($info);
                $infoStr = str_replace("\n", "\n  ", $infoStr);
                echo "  ---\n  $infoStr\n  ...\n";
            } else {
                $infoStr = json_encode(
                    $info,
                    JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE,
                );
                $infoStr = str_replace("\n", "\n# ", $infoStr);
                echo "# $infoStr\n";
            }
        } else {
            echo '# ' . str_replace("\n", "\n# ", (string) $ex) . "\n";
        }
    }
}

// vim: set sts=4 sw=4 et :
