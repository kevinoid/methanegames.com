<#
.SYNOPSIS
Changes the base URL used by TriPeaksLite.xap.
#>

param(
    [Parameter(HelpMessage='Current base URL used by TriPeaksLite.xap',
        Mandatory=$true,
        Position=0)]
    [ValidateNotNullOrEmpty()]
    [string]$Url,

    [Parameter(HelpMessage='New base URL to use in TriPeaksLite.xap',
        Mandatory=$true,
        Position=1)]
    [ValidateNotNullOrEmpty()]
    [string]$NewUrl,

    [Parameter(HelpMessage='Path to TriPeaksLite.xap',
        ValueFromPipeline=$true)]
    [ValidateNotNullOrEmpty()]
    [string]$Path = 'TriPeaksLite.xap'
)

# Stop on all errors
$ErrorActionPreference = 'Stop'
Set-StrictMode -Version Latest

# Creates a new directory for storing temporary files
# Based on https://stackoverflow.com/a/34559554
function New-TemporaryDirectory {
    Param([string]$Prefix = '')

    $parent = [System.IO.Path]::GetTempPath()
    $name = $prefix + [System.Guid]::NewGuid()
    New-Item -ItemType Directory -Path (Join-Path $parent $name)
}

# Changes the base URL in a .resources file
Function Edit-Resources {
    Param(
        [Parameter(HelpMessage='Path of .resources file to edit',
            Mandatory=$true,
            Position=0,
            ValueFromPipeline=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$Path)

    $tempFileName = [System.Guid]::NewGuid().ToString() + '.resources'
    $tempPath = Join-Path (Split-Path $Path) $tempFileName
    $changed = $false

    $reader = New-Object -TypeName System.Resources.ResourceReader -ArgumentList $Path
    try {
        $writer = New-Object -TypeName System.Resources.ResourceWriter -ArgumentList $tempPath

        $enum = $reader.GetEnumerator()
        while ($enum.MoveNext()) {
            [string]$name = $enum.Key
            if ($name -like '*.xaml') {
                $value = $enum.Value
                $stream = $value -as [System.IO.Stream]
                if ($stream -ne $null) {
                    # FIXME: Use <?xml encoding ?>, if any
                    # Current resources all have UTF-8 BOM without <?xml ?>
                    $streamReader = New-Object -TypeName System.IO.StreamReader `
                        -ArgumentList $stream,$true
                    try {
                        $oldXaml = $streamReader.ReadToEnd()
                        $encoding = $streamReader.CurrentEncoding
                    } finally {
                        $streamReader.Dispose()
                    }

                    $newXaml = $oldXaml.Replace($Url, $NewUrl)
                    $changed = $changed -or ($newXaml -ne $oldXaml)

                    # Note: Can't use $encoding.GetBytes(), doesn't include BOM
                    $newStream = New-Object -TypeName System.IO.MemoryStream
                    $newWriter = New-Object -TypeName System.IO.StreamWriter `
                        -ArgumentList $newStream,$encoding
                    $newWriter.Write($newXaml)
                    $newWriter.Flush()
                    $newStream.Position = 0

                    $value = $newStream
                } else {
                    Write-Warning "Skipping non-Stream resource $name"
                }
            } else {
                Write-Warning "Skipping non-XAML resource $name"
            }

            $writer.AddResource($name, $value)
        }
    } finally {
        if ($writer -ne $null) { $writer.Dispose() }
        $reader.Dispose()
    }

    if (-not $changed) {
        Write-Warning "$Url not found in $Path"
    }

    Move-Item -Force -LiteralPath $tempPath -Destination $Path
}

# Check ildasm is on $PATH
if (-Not (Get-Command ildasm -errorAction SilentlyContinue))
{
    Write-Error -Category InvalidOperation 'ildasm not found in $PATH.  Run from Developer Command Prompt?'
    exit 1
}

# Create a temporary directory in which to work
[string] $tempDir = New-TemporaryDirectory 'Edit-TriPeaksLite-'

try {
    # Create a copy of TriPeaksLite.xap with a .zip extension
    $zipPath = Join-Path $tempDir TriPeaksLite.zip
    Copy-Item -LiteralPath $Path -Destination $zipPath

    # Extract TriPeaksLite.dll from the zip
    $shell = New-Object -ComObject Shell.Application
    $zip = $shell.NameSpace($zipPath)
    if ($zip -eq $null) {
        Write-Error -Category OpenError "Unable to open $zipPath"
        exit 1
    }

    # FIXME: Iterating the files in the zip seems like a waste.
    foreach ($item in $zip.items()) {
        if ($item.Name -eq 'TriPeaksLite.dll') {
            $shell.Namespace($tempDir).CopyHere($item)
            break
        }
    }

    # Disassemble TriPeaksLite.dll
    $dllPath = Join-Path $tempDir TriPeaksLite.dll
    $ilPath = Join-Path $tempDir TriPeaksLite.il
    ildasm $dllPath "/out=$ilPath" /utf8 /typelist /all

    # Change URL in the disassembled IL and save it
    $oldIL =[System.IO.File]::ReadAllText($ilPath)
    $newIL = $oldIL.Replace($Url, $NewUrl)
    if ($newIL -eq $oldIL) {
        Write-Error -Category InvalidArgument "$Url not found in TriPeaksLite.il"
        exit 1
    }
    Set-Content -LiteralPath $ilPath -Value $newIL -Encoding UTF8

    # Change URL in .resources files
    Get-ChildItem $tempDir -Filter *.resources |
        ForEach-Object { Edit-Resources $_.FullName }

    # Assemble a new DLL from the modified IL and Resources
    Remove-Item -LiteralPath $dllPath
    ilasm $ilPath /dll

    # Copy the new DLL into the zip
    #
    # FIXME: Copying done asynchronously by shell, with overwrite prompt,
    # no easy way to determine completion or success/failure
    #
    # The original TriPeaksLite.xap is "invalid or corrupted"
    # Info-Zip warns "Local Entry name does not match CD"
    $zip.CopyHere($dllPath)
    $copySuccess = Read-Host -Prompt "Did copying succeed (y/n)"

    if ($copySuccess -like 'y*') {
        # Replace the xap with the updated zip
        Move-Item -Force -LiteralPath $zipPath -Destination $Path
    } else {
        Move-Item -Force -LiteralPath $dllPath (Split-Path -Resolve $Path)
    }
} finally {
    # Remove the temporary directory
    Remove-Item -LiteralPath $tempDir -Force -Recurse
}
